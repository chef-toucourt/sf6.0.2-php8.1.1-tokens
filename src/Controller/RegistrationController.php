<?php

namespace App\Controller;

use App\Controller\Trait\RegistrationControllerHelperTrait;
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Repository\TokenRepository;
use App\Repository\UserRepository;
use App\Security\AppAuthenticator;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

/**
 * @author SF but edited by Chef-Toucourt
 */
final class RegistrationController extends AbstractController
{
    use RegistrationControllerHelperTrait;

    public const ROUTES = [
        'app_confirm_account' => 'app_confirm_account',
        'app_register'        => 'app_register',
    ];

    #[
        Route(
            path: '/inscription',
            name: self::ROUTES['app_register'],
            methods: [
                Request::METHOD_GET,
                Request::METHOD_POST
            ]
        )
    ]
    /**
     * @throws ORMException
     * @throws Exception
     */
    public function register(
        MailerService $mailer,
        Request $request,
        TokenRepository $tokenRepository,
        UrlGeneratorInterface $urlGenerator,
        UserPasswordHasherInterface $userPasswordHasher,
        UserRepository $userRepository
    ): Response
    {
        $user = new User();

        $form = $this->createForm(
            type: RegistrationFormType::class,
            data: $user
        );

        $form->handleRequest(
            request: $request
        );

        if (
            $form->isSubmitted()
            && $form->isValid()
        ) {
            $encodedPassword = $this->getEncodedPassword(
                form: $form,
                user: $user,
                userPasswordHasher: $userPasswordHasher
            );

            $userRepository->persistAndFlushUserInstance(
                user: $user,
                encodedPassword: $encodedPassword
            );

            $confirmationUrl = $this->getConfirmationUrl(
                request: $request,
                tokenRepository: $tokenRepository,
                urlGenerator: $urlGenerator,
                user: $user
            );

            $mailer->sendEmail(
                recipient: $user->getEmail(),
                subject: 'Veuillez activer votre compte utilisateur',
                htmlTemplate: 'registration/email/registration-email.html.twig',
                context: [
                    'confirmation_url' => $confirmationUrl
                ]
            );

            return $this->redirectToRoute(
                route: 'app_login'
            );
        }

        return $this->renderForm(
            view: 'registration/register.html.twig',
            parameters: [
                'registrationForm' => $form,
            ]
        );
    }

    #[
        Route(
            path: '/confirmation-compte-utilisateur',
            name: self::ROUTES['app_confirm_account'],
            methods: [
                Request::METHOD_GET
            ]
        )
    ]
    /**
     * @throws ORMException
     */
    public function confirmAccount(
        AppAuthenticator $appAuthenticator,
        EventDispatcherInterface $eventDispatcher,
        Request $request,
        TokenRepository $tokenRepository,
        UserAuthenticatorInterface $userAuthenticator,
        UserRepository $userRepository
    ): Response
    {
        $token = $this->getConfirmationTokenOrThrowAnException(
            request: $request
        );

        $token = $this->getTokenWithUserOrThrowAnException(
            tokenRepository: $tokenRepository,
            token: $token
        );

        $user = $token->getUser();

        try {
            $userRepository->updateUserAccountEnabledProperty(
                user: $user
            );

            $tokenRepository->resetTokenValueAfterSuccessfulAction(
                token: $token
            );
        } catch (OptimisticLockException | ORMException | Exception $error) {
            throw new ORMException(
                message: $error
            );
        }

        return $userAuthenticator->authenticateUser(
            user: $user,
            authenticator: $appAuthenticator,
            request: $request
        );
    }
}
