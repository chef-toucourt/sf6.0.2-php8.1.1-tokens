<?php

namespace App\Controller\Trait;

use App\Controller\RegistrationController;
use App\Controller\SecurityController;
use App\Entity\Token;
use App\Entity\User;
use App\Repository\TokenRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use LogicException;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

trait RegistrationControllerHelperTrait
{
    private function getEncodedPassword(
        FormInterface $form,
        User $user,
        UserPasswordHasherInterface $userPasswordHasher
    ): string
    {
        return $userPasswordHasher->hashPassword(
            user: $user,
            plainPassword: $form
                                ->get(
                                    name: 'plainPassword'
                                )
                                ->getData()
        );
    }

    /**
     * @throws Exception
     */
    private function getConfirmationURL(
        Request $request,
        TokenRepository $tokenRepository,
        UrlGeneratorInterface $urlGenerator,
        User $user
    ): string
    {
        $confirmAccountToken = $tokenRepository->generateConfirmationTokenForThisUserAndThisType(
            user: $user,
            type: Token::TYPES['ACCOUNT_CONFIRMATION']
        );

        return $urlGenerator->generate(
            name: RegistrationController::ROUTES['app_confirm_account'],
            parameters: [
                'token'   => $confirmAccountToken,
            ],
            referenceType: UrlGeneratorInterface::ABSOLUTE_URL
        );
    }

    private function getConfirmationTokenOrThrowAnException(Request $request): string
    {
        $token = $request
            ->query
            ->get(
                key: 'token'
            );

        if (
            is_string(
                value: $token
            ) === false
        ) {
            throw new AccessDeniedException(
                message: 'No valid token, no access Bro.'
            );
        }

        return $token;
    }

    /**
     * @throws NonUniqueResultException
     */
    private function getTokenWithUserOrThrowAnException(
        TokenRepository $tokenRepository,
        ?string $token = null
    ): Token
    {
        $token = $tokenRepository->getTokenWithUserIfConfirmationTokenIsValidOrNull(
            Token::TYPES['ACCOUNT_CONFIRMATION'],
            $token
        );

        if ($token instanceof Token === false) {
            throw $this->createNotFoundException(
                message: 'User not found.'
            );
        }

        return $token;
    }
}