<?php

namespace App\Controller;

use LogicException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @author SF but edited by Chef-Toucourt
 */
final class SecurityController extends AbstractController
{
    public const ROUTES = [
        'app_login' => 'app_login',
        'app_logout' => 'app_logout',
    ];

    #[
        Route(
            path: '/connexion',
            name: self::ROUTES['app_login'],
            methods: [
                Request::METHOD_GET,
                Request::METHOD_POST
            ]
        )
    ]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            view: 'security/login.html.twig',
            parameters: [
                'error'         => $error,
                'last_username' => $lastUsername,
            ]
        );
    }

    #[
        Route(
            path: '/deconnexion',
            name: self::ROUTES['app_logout'],
            methods: [
                Request::METHOD_GET,
            ]
        )
    ]
    public function logout(): void
    {
        throw new LogicException(
            message: 'This method can be blank - it will be intercepted by the logout key on your firewall.'
        );
    }
}
