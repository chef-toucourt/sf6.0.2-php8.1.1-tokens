<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[
    Entity(
        repositoryClass: UserRepository::class
    ),
    Table(
        name: 'users'
    ),
    UniqueEntity(
        fields: [
            'email'
        ],
        message: 'Sorry Bro, impossible de créer un compte utilisateur avec cet email :-('
    )
]
/**
 * @author SF but edited by Chef-Toucourt
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[
        Id,
        GeneratedValue,
        Column(
            type: 'integer'
        )
    ]
    private int $id;

    #[
        Column(
            type: Types::BOOLEAN
        )
    ]
    private bool $isAccountEnabled;

    #[
        Column(
            type: Types::JSON,
        )
    ]
    private array $roles = [];

    #[
        Column(
            type: Types::STRING,
            length: 180,
            unique: true
        )
    ]
    private string $email;

    #[
        Column(
            type: Types::STRING,
        )
    ]
    private string $password;

    public function __construct()
    {
        $this->isAccountEnabled = false;
        $this->roles = ["ROLE_USER"];
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function isAccountEnabled(): bool
    {
        return $this->isAccountEnabled;
    }

    public function setAccountEnabled(bool $accountEnabled): self
    {
        $this->isAccountEnabled = $accountEnabled;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function eraseCredentials()
    {
    }
}
