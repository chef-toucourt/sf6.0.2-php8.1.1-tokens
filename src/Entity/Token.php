<?php

namespace App\Entity;

use App\Repository\TokenRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\Table;
use Exception;

#[
    Entity(
        repositoryClass: TokenRepository::class
    ),
    Table(
        name: 'tokens'
    )
]
/**
 * @author Chef-Toucourt
 */
class Token
{
    public const TYPES = [
        'ACCOUNT_CONFIRMATION' => 'ACCOUNT_CONFIRMATION',
    ];

    #[
        Id,
        GeneratedValue,
        Column(type: Types::INTEGER)
    ]
    private int $id;

    #[
        Column(
            type: Types::INTEGER
        )
    ]
    private int $dieAt;

    #[
        Column(
            type: Types::STRING,
            length: 255,
            unique: true,
            nullable: true
        )
    ]
    private ?string $value = null;

    #[
        Column(
            type: Types::STRING,
            length: 255
        )
    ]
    private string $type;

    #[
        ManyToOne(
            targetEntity: User::class
        ),
        JoinColumn(nullable: false)
    ]
    private User $user;

    /**
     * @throws Exception
     */
    public function __construct(
        User $user,
        string $type,
        int $timeInSeconds = 300
    )
    {
        $this->dieAt = time() + $timeInSeconds;
        $this->type = $type;
        $this->user = $user;
        $this->value = bin2hex(
                string: random_bytes(
                    length: 16
                )
        ) . time();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @throws Exception
     */
    public function resetValue(): self
    {
        $this->value = null;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getDieAt(): int
    {
        return $this->dieAt;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
