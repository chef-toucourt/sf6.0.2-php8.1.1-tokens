<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * @author SF but edited by Chef-Toucourt
 */
class AppAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    public function __construct(
        private UrlGeneratorInterface $urlGenerator
    )
    {}

    public function authenticate(Request $request): Passport
    {
        $email = $request
                        ->request
                        ->get(
                            key: 'email',
                            default: ''
                        );

        $request
                ->getSession()
                ->set(
                    name: Security::LAST_USERNAME,
                    value: $email
                );

        return new Passport(
            userBadge: new UserBadge(
                userIdentifier: $email
            ),
            credentials: new PasswordCredentials(
                password: $request
                                ->request
                                ->get(
                                    key: 'password',
                                    default: ''
                                )
            ),
            badges: [
                new CsrfTokenBadge(
                    csrfTokenId: 'authenticate',
                    csrfToken: $request
                                    ->request
                                    ->get(
                                        key: '_csrf_token'
                                    )
                ),
            ]
        );
    }

    public function onAuthenticationSuccess(
        Request $request,
        TokenInterface $token,
        string $firewallName
    ): ?Response
    {
        if ($targetPath = $this->getTargetPath(
                session: $request->getSession(),
                firewallName: $firewallName
            )
        ) {
            return new RedirectResponse(
                url: $targetPath
            );
        }

        return new RedirectResponse(
            url: $this->getLoginUrl(
                request: $request
            )
        );
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this
                    ->urlGenerator
                    ->generate(
                        name: self::LOGIN_ROUTE
                    );
    }
}
