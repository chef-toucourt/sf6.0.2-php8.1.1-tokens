<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @author SF but edited by Chef-Toucourt
 */
class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user): void
    {}

    public function checkPostAuth(UserInterface $user): void
    {
        if ($user instanceof User === false) {
            return;
        }

        if ($user->isAccountEnabled() === false) {
            throw new CustomUserMessageAuthenticationException(
                message: 'NO NO NO BRO... il te faut activer ton compte utilisateur avant !'
            );
        }
    }
}