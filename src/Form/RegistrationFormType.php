<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

/**
 * @author SF but edited by Chef-Toucourt
 */
final class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                child: 'email',
                type: EmailType::class,
                options: [
                    'attr' => [
                        'placeholder'  => 'Email'
                    ],
                    'constraints' => [
                        new Email(
                            options: [
                                'message' => 'Fournis ton email Bro...'
                            ]
                        )
                    ],
                    'label' => false,
                ]
            )
            ->add(
                child: 'plainPassword',
                type: PasswordType::class,
                options: [
                    'attr' => [
                        'autocomplete' => 'new-password',
                        'placeholder'  => 'Mot de passe'
                    ],
                    'constraints' => [
                        new NotBlank(
                            options: [
                                'message' => 'WTF Bro ?!',
                            ]
                        ),
                        new Regex(
                            pattern: [
                                'pattern' => '/^(?=.*[a-zà-ÿ])(?=.*[A-ZÀ-Ý])(?=.*[0-9])(?=.*[^a-zà-ÿA-ZÀ-Ý0-9]).{12,255}$/',
                                'message' => 'Je te prie de bien vouloir fournir un mot de passe un minimum potable Bro...',
                            ]
                        ),
                    ],
                    'label'  => false,
                    'mapped' => false,
                ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            defaults: [
                'data_class' => User::class,
            ]
        );
    }
}
