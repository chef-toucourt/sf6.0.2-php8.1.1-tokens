<?php

namespace App\Service;

use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;

/**
 * @author Chef-Toucourt
 */
final class MailerService
{
    public function __construct(
        private MailerInterface $mailer,
        private string $fromEmail,
    ) {}

    public function sendEmail(
        string $recipient,
        string $subject,
        string $htmlTemplate,
        array $context
    ): TemplatedEmail
    {
        $templatedEmail = new TemplatedEmail();

        $templatedEmail
            ->from(
                addresses: new Address(
                    address: $this->fromEmail,
                )
            )
            ->to(
                addresses: $recipient
            )
            ->subject(
                subject: $subject
            )
            ->htmlTemplate(
                template: $htmlTemplate
            )
            ->context(
                context: $context
            );

        try {
            $this
                ->mailer
                ->send(
                    message: $templatedEmail
                );
        } catch (TransportExceptionInterface $error) {
            throw new TransportException(
                message: $error
            );
        }

        return $templatedEmail;
    }
}
