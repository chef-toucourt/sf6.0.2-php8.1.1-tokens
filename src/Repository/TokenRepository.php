<?php

namespace App\Repository;

use App\Entity\Token;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * @method Token|null find($id, $lockMode = null, $lockVersion = null)
 * @method Token|null findOneBy(array $criteria, array $orderBy = null)
 * @method Token[]    findAll()
 * @method Token[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @author SF but edited by Chef-Toucourt
 */
class TokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct(
            registry: $registry,
            entityClass: Token::class
        );
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws Exception
     */
    public function resetTokenValueAfterSuccessfulAction(
        Token $token
    ): void
    {
        $token->resetValue();

        $this
            ->_em
            ->flush();
    }

    /**
     * @throws Exception
     */
    public function generateConfirmationTokenForThisUserAndThisType(
        User $user,
        string $type
    ):string
    {
        $token = new Token(
            user: $user,
            type: $type
        );

        /*
         * Note: put a try catch for this: ORMException or Exception in entity constructor... ;-)
         */
        $this
            ->_em
            ->persist($token);

        $this
            ->_em
            ->flush();

        return $token->getValue();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getTokenWithUserIfConfirmationTokenIsValidOrNull(
        string $type,
        ?string $token
    ): ?Token
    {
        return $this
                    ->createQueryBuilder(
                        alias: 't'
                    )
                    ->select(
                        select: 't, user'
                    )
                    ->innerJoin(
                        join: 't.user',
                        alias: 'user'
                    )
                    ->where(
                        predicates: 't.value = :token'
                    )
                    ->andWhere('t.dieAt > :timestamp')
                    ->andWhere('t.type = :type')
                    ->setParameters(
                        parameters: [
                            'timestamp' => time(),
                            'token'     => $token,
                            'type'      => $type,
                        ]
                    )
                    ->getQuery()
                    ->getOneOrNullResult();
    }
}
